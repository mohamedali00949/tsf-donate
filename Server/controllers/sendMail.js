const nodemailer = require("nodemailer");
const dotenv = require("dotenv");
const { google } = require("googleapis");
const { OAuth2 } = google.auth;

const OAUTH_PLAYGROUND = "https://developers.google.com/oauthplayground";

dotenv.config();

const {
  MAILING_SERVICE_CLIENT_ID,
  MAILING_SERVICE_CLIENT_SECRET,
  MAILING_SERVICE_REFRESH_TOKEN,
  SENDER_EMAIL_ADDRESS,
  GOOGLE_PASSWORD,
} = process.env;

const oauth2Client = new OAuth2(
  MAILING_SERVICE_CLIENT_ID,
  MAILING_SERVICE_CLIENT_SECRET,
  MAILING_SERVICE_REFRESH_TOKEN,
  SENDER_EMAIL_ADDRESS,
  OAUTH_PLAYGROUND
);

oauth2Client.setCredentials({
  refresh_token: MAILING_SERVICE_REFRESH_TOKEN,
});

const validateEmail = (email) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

function validatePhoneNumber(input_str) {
  var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

  return re.test(input_str);
}

const sendMail = async (req, res) => {
  try {
    const { email, phone, name, amount } = req.body;

    if (!email || !phone || !name || !amount)
      return res
        .status(400)
        .json({ message: "Please fill all required fields." });

    if (!validateEmail(email))
      return res.status(400).json({ message: "Invalid email." });
    if (!validatePhoneNumber(phone))
      return res.status(400).json({ message: "Invalid phone." });

    const accessToken = await oauth2Client.getAccessToken();

    const smtpTransport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: SENDER_EMAIL_ADDRESS,
        clientId: MAILING_SERVICE_CLIENT_ID,
        clientSecret: MAILING_SERVICE_CLIENT_SECRET,
        refreshToken: MAILING_SERVICE_REFRESH_TOKEN,
        accessToken: accessToken,
      },
    });

    const mailOptions = {
      from: "Accept payments",
      to: email,
      subject: `Thank you for donate money`,
      html: `
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                </head>
                <body>
                    <div style="max-width: 700px; margin:auto; border: 2px solid #ddd; padding: 50px 20px; text-align: left; font-size: 110%; background: #f5f7fa;">
                        <h2 style="text-align: center; color: #1976d2; font-size: 40px;">Welcome to TSF Donate</h2>
                        <h3>Hi ${name}</h3>
                        <p style="border: 1px solid #ccc; border-radius: 5px; padding: 10px; margin-block: 10px; box-shadow: 0 0 5px 0px #e3d8d8; background: white;">Name: <strong>${name}</strong></p>
                        <p style="border: 1px solid #ccc; border-radius: 5px; padding: 10px; margin-block: 10px; box-shadow: 0 0 5px 0px #e3d8d8; background: white;">Email: <strong>${email}</strong></p>
                        <p style="border: 1px solid #ccc; border-radius: 5px; padding: 10px; margin-block: 10px; box-shadow: 0 0 5px 0px #e3d8d8; background: white;">Phone: <strong>${phone}</strong></p>
                        <p style="border: 1px solid #ccc; border-radius: 5px; padding: 10px; margin-block: 10px; box-shadow: 0 0 5px 0px #e3d8d8; background: white;">Amount: <strong>${amount}</strong></p>
                        <p>
                          Your money is accepted.<br>
                          Thank you
                        </p>
                    </div>
                </body>
            </html>
        `,
    };

    const result = await smtpTransport.sendMail(mailOptions);
    // await console.log(result);
    return res.status(200).json(result);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: error.message });
  }
};

module.exports = sendMail;
