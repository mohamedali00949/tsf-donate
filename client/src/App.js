import React from "react";

import { BrowserRouter, Route, Routes } from "react-router-dom";

import { Container } from "@mui/material";

import Checkout from "./Components/Checkout/Checkout";
import Home from "./Components/Home/Home";
import Navbar from "./Components/Navbar/Navbar";

import NotFoundPage from "./Components/NotFoundPage/NotFoundPage";

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Container maxWidth="lg" style={{marginTop: "34px"}}>
        <Routes>
            <Route index path="/" exact element={<Home />} />
            <Route path="/donate" exact element={<Checkout />} />
            <Route path="*" exact element={<NotFoundPage />} />
        </Routes>
      </Container>
    </BrowserRouter>
  );
}

export default App;
