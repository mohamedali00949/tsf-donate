import axios from 'axios';

const sendMail = (data) => axios.post('https://tsf-payment-gateway-integratio.herokuapp.com/send_mail/', data)

export default sendMail;