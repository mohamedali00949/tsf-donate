import React from "react";

import { NavLink, Link } from "react-router-dom";

import { Container, AppBar, Toolbar, Typography } from "@mui/material";

import useStyles from "./styles";

import logo from "./donation.svg";

function Navbar() {
  const classes = useStyles();

  return (
    <AppBar
      className={classes.appBar}
      style={{ flexDirection: "row" }}
      position="sticky"
    >
      <Container maxWidth="lg" style={{ display: "contents" }}>
        <Link to="/" className={classes.link}>
          <div className={classes.logo}>
            <img
              src={logo}
              alt="logo"
              width="40px"
              height="40px"
              style={{ marginRight: "10px" }}
            />
            <Typography>TSF Donate</Typography>
          </div>
        </Link>
        <Toolbar className={classes.toolbar}>
          <Typography component={NavLink} to="/" className={classes.link}>
            Home
          </Typography>
          <Typography component={NavLink} to="/donate" className={classes.link}>
            Donate
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default Navbar;
