import React from 'react';

import { Typography, Button } from '@mui/material';
import { Link } from 'react-router-dom';

import {FaDonate} from 'react-icons/fa';

function Home() {
  return (
    <div style={{display: "flex", alignItems: "center", flexDirection: "column", width: "100%",height: "100%", background: "white", padding: "50px", border: '1px solid #ccc', borderRadius: "10px", boxShadow: "0 0 4px 0px #ccc"}}>
        <Typography variant="h2" align="center">
            Donate With us
        </Typography>
        <Button component={Link} to="/donate" variant="contained" startIcon={<FaDonate />}>Donate</Button>
    </div>
  )
}

export default Home;