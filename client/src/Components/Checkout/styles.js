import {makeStyles} from '@mui/styles';

export default makeStyles(() => ({
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        background: '#f0f8ff73',
        zIndex: '1000',
    },
    innerContainer: {
        padding: '20px',
        background: "white",
        marginInline: "25px",
        boxShadow: '0 0 4px #ccc',
        textAlign: "center",
        borderRadius: "10px",
    },
    buttons : {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        margin: '10px',
    },
    form : {
        marginTop: '10px',
        textAlign: 'center',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    }
}))