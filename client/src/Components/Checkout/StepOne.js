import React from "react";

import useStyles from "./styles";

import Aler from "../Util/Aler";

import { Button, Typography, TextField, InputAdornment } from "@mui/material";
import { Link } from "react-router-dom";

import { useForm } from "react-hook-form";

function StepOne({ nextStep, setDonateData, donateData }) {
  const classes = useStyles();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const handleSubmitData = async (data) => {
    console.log(data);
    await setDonateData({...donateData, amount: Number(data.amount)});
    await nextStep();
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit(handleSubmitData)}>
      {errors.amount && (
          <Aler
            message={errors.amount.message}
            type="error"
            title="Error"
            className="authError"
            style={{ top: "80px" }}
          />
        )}
      <Typography variant="h2" style={{ fontSize: "30px"}}>Select amount</Typography>
      <div style={{width: "100%", marginBlock: "10px"}}>
        <TextField
          {...register("amount", {
            required: "The amount is required",
            min: { value: 1, message: "The amount must be larger than 0." },
          })}
          defaultValue={donateData.amount}
          label="Amount"
          variant="outlined"
          type="number"
          required
          InputProps={{
            startAdornment: <InputAdornment position="start">$</InputAdornment>,
          }}
        />
      </div>
      <div className={classes.buttons}>
        <Button
          type="button"
          variant="outlined"
          color="primary"
          style={{ textTransform: "capitalize" }}
          component={Link}
          to="/"
        >
          back to home
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          style={{ textTransform: "capitalize" }}
        >
          next
        </Button>
      </div>
    </form>
  );
}

export default StepOne;
