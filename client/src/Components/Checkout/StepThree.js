import React, { useEffect } from 'react';

import useStyles from "./styles";

import { Button, Typography } from "@mui/material";

import sendMail from "../../api";

function StepThree({backStep, nextStep, setIsLoading, donateData, setError}) {
    const classes = useStyles();

    useEffect(() => {
      window.paypal.Buttons({
        enableStandardCardFields: true,
        createOrder: function(data, actions) {
          return actions.order.create({
            payer: {
              name: {
                given_name: donateData.name.split(' ')[0],
                surname: donateData.name.split(' ')[1]
              },
              address: {
                address_line_1: 'fdfg dfjgj djgrfjg',
                address_line_2: '',
                admin_area_2: 'asdsff',
                admin_area_1: 'safdf',
                postal_code: '95121',
                country_code: 'EG'
              },
              email_address: donateData.email,
              phone: {
                phone_type: "MOBILE",
                phone_number: {
                  national_number: donateData.phone,
                }
              }
            },
            purchase_units: [
              {
                amount: {
                  value: donateData.amount,
                },
                shipping: {
                  address: {
                    address_line_2: '',
                    address_line_1: "sdgfhtghg dgdhyt dfsygtgyh",
                    admin_area_2: "Cairo",
                    admin_area_1: "EG_zip = 20",
                    postal_code: "20",
                    country_code: "EG"
                  }
                },
              }
            ]
          });
        },

        // Finalize the transaction after payer approval
        onApprove: function(data, actions) {
          return actions.order.capture().then(async function(orderData) {
            // Successful capture! For dev/demo purposes:
            console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
            var transaction = orderData.purchase_units[0].payments.captures[0];
            await setIsLoading(true);
            if (transaction.status === 'COMPLETED') {
              await sendMail(donateData).then(() => nextStep()).catch((error) => {setError(error?.response?.data?.message || 'Something went wrong at send mail'); setIsLoading(false);});
            }
            await setIsLoading(false);
          });
        }
      }).render('#paypal-button-container');
    }, []); // eslint-disable-line

  return (
    <form className={classes.form}>
      <Typography variant="h2" style={{ fontSize: "30px"}}>Payment Method</Typography>
      <div id="paypal-button-container" style={{ width: "100%", marginBlock: "10px"}}></div>
      <div className={classes.buttons} style={{justifyContent: "center"}}>
        <Button
          type="button"
          variant="outlined"
          color="primary"
          style={{ textTransform: "capitalize" }}
          onClick={() => backStep()}
        >
          back
        </Button>
      </div>
    </form>
  )
}

export default StepThree