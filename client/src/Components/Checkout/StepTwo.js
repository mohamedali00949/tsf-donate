import React from 'react';

import useStyles from "./styles";

import { Button, Typography, TextField } from "@mui/material";

import {useForm} from 'react-hook-form';

import Aler from '../Util/Aler';

function StepTwo({backStep, nextStep, donateData, setDonateData}) {
  const classes = useStyles();
  const {register, formState: {errors}, handleSubmit} = useForm();

  const emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const phonePattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im; //eslint-disable-line

  const handleSubmitData = (data) => {
    console.log(Object.assign(donateData, data));
    setDonateData(Object.assign(donateData, data));

    nextStep();
  }

  return (
    <form className={classes.form} onSubmit={handleSubmit(handleSubmitData)}>
      {errors.name && errors.email && errors.phone && (
        <Aler
          message={errors.name.message || errors.email.message || errors.phone.message}
          type="error"
          title="Error"
          className="authError"
          style={{ top: "80px" }}
        />
      )}
      <Typography variant="h2" style={{ fontSize: "30px"}}>Enter Personal Details</Typography>
      <div style={{width: "100%", marginBlock: "10px"}}>
        <TextField style={{width: 'inherit'}} variant="outlined" type="text" required label="Name" defaultValue={donateData.name} {...register('name', {required: "Enter The full name"})} />
      </div>
      <div style={{width: "100%", marginBlock: "10px"}}>
        <TextField style={{width: 'inherit'}} variant="outlined" type="email" required label="Email" defaultValue={donateData.email} {...register('email', {required: "Enter The email", pattern: {value: emailPattern, message: "Invalid email address."}})} />
      </div>
      <div style={{width: "100%", marginBlock: "10px"}}>
        <TextField style={{width: 'inherit'}} variant="outlined" type="tel" required label="Phone Number" defaultValue={donateData.phone} {...register('phone', {required: "Enter The phone number", pattern: {value: phonePattern, message: "Invalid phone number"}})} />
      </div>
      <div className={classes.buttons}>
        <Button
          type="button"
          variant="outlined"
          color="primary"
          style={{ textTransform: "capitalize" }}
          onClick={() => backStep()}
        >
          back
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          style={{ textTransform: "capitalize" }}
        >
          next
        </Button>
      </div>
    </form>
  )
}

export default StepTwo