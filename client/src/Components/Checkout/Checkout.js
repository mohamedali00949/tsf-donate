import React, { useState, useEffect } from "react";

import useStyles from "./styles";

import { Link } from "react-router-dom";

import {
  Box,
  Stepper,
  Step,
  StepLabel,
  Typography,
  Button,
} from "@mui/material";

import Aler from "../Util/Aler";
import Loading from "../Util/loading";

import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import StepThree from "./StepThree";

function Checkout() {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [donateData, setDonateData] = useState({
    amount: 0,
    name: "",
    email: "",
    phone: "",
  });
  const [activeStep, setActiveStep] = useState(0);
  const steps = ["select amount", "enter personal details", "Payment Method"];

  useEffect(() => {
    var PAYPAL_SCRIPT = 'https://www.paypal.com/sdk/js?client-id=Ad-cTRtgcQ0T20IcuaNAaZ2EiixQZgQxedtU_2xmr62B3ddCRva9QAWPpEakAjXVSSU7fAuI-WdIdFbn&currency=USD';
    var script = document.createElement('script');
    script.setAttribute('src', PAYPAL_SCRIPT);
    window.document.head.appendChild(script);
  }, [])

  const Confirmation = () => (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <Typography
            variant="h2"
            style={{ textAlign: "center", fontSize: "45px", fontWeight: "400" }}
          >
            Succeed Donate
          </Typography>
          <Typography>Thanks</Typography>
          <Button
            style={{ marginTop: "10px" }}
            component={Link}
            to="/"
            color="primary"
            variant="outlined"
          >
            Back to home
          </Button>
        </>
      )}
    </>
  );

  const nextStep = () => setActiveStep((prev) => prev + 1);
  const backStep = () => setActiveStep((prev) => prev - 1);

  const Form = () =>
    activeStep === 0 ? (
      <StepOne
        nextStep={nextStep}
        setDonateData={setDonateData}
        donateData={donateData}
      />
    ) : (
      <>
        {" "}
        {activeStep === 1 ? (
          <StepTwo
            nextStep={nextStep}
            backStep={backStep}
            setDonateData={setDonateData}
            donateData={donateData}
          />
        ) : (
          <StepThree
            nextStep={nextStep}
            backStep={backStep}
            setIsLoading={setIsLoading}
            donateData={donateData}
            setError={setError}
          />
        )}
      </>
    );

  return (
    <div className={classes.container}>
      {error && (
        <Aler
          message={error}
          type="error"
          title="Error"
          className="authError"
          style={{ top: "80px" }}
        />
      )}
      <div className={classes.innerContainer}>
        <Box sx={{ width: "100%" }}>
          <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel style={{ textTransform: "capitalize" }}>
                  {label}
                </StepLabel>
              </Step>
            ))}
          </Stepper>
          {isLoading ? (
            <Loading />
          ) : (
            <>{activeStep === steps.length ? <Confirmation /> : <Form />}</>
          )}
        </Box>
      </div>
    </div>
  );
}

export default Checkout;
