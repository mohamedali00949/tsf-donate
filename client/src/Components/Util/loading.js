import React from "react";
import { CircularProgress, Paper } from "@mui/material";

function Loading() {
  return (
    <Paper
      elevation={0}
      style={{
        backgroundColor: "transparent",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: "20px",
        borderRadius: "15px",
        height: "39vh",
        boxShadow: "none",
      }}
    >
      <CircularProgress size="7em" />
    </Paper>
  );
}

export default Loading;
