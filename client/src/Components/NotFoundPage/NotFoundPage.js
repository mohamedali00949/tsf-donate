import { Button, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import image from "./404.svg";

function NotFoundPage() {
  return (
    <div
      style={{
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      <Typography variant="h2">This page is not found.</Typography>
      <Button
        component={Link}
        color="primary"
        to="/"
        variant="contained"
        style={{ width: "fit-content", alignSelf: "center" }}
      >
        Go to Home
      </Button>
      <img
        loading="lazy"
        src={image}
        alt="404"
        style={{ alignSelf: "center", width: "60%" }}
      />
    </div>
  );
}

export default NotFoundPage;
